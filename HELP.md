<h2>10 Giây</h2>

<p>10 Giây- Sân chơi thử thách trí tuệ - tương tác trực tuyến. </p>

<p>Chỉ với một chiếc điện thoại thông minh, người dùng ở khắp mọi nơi đều có thể tham gia trả lời các câu hỏi thách đố trong chương trình để nhận về những giải thưởng tiền mặt cực kì hấp dẫn. 
Luật chơi vô cùng đơn giản: Vượt qua 10 câu hỏi thử thách ở nhiều lĩnh vực và các cấp độ dễ - khó khác nhau; với nguyên tắc đúng đi tiếp, sai dừng lại, cùng với đó là rất nhiều tính năng thú vị sẽ giúp người chơi dễ dàng đạt được giải thưởng.</p>

<p>Giải thưởng: <b>20 triệu đồng</b> chia đều cho các khán giả trả lời đúng 10 câu hỏi của chương trình.</p>

<p>Thời gian: <b>14h các ngày từ thứ 2 đến thứ 6 và 20h30 các ngày thứ 7 chủ nhật hàng tuần.</b></p>

<p>Hãy tham gia ngay với chúng tôi và đừng trễ nhé!</p>

<h2>Luật chơi và trái tim thêm lượt</h2>

<p>Luật chơi vô cùng đơn giản: Vượt qua 10 câu hỏi thử thách ở nhiều lĩnh vực và các cấp độ dễ - khó khác nhau; với nguyên tắc đúng đi tiếp, sai dừng lại, cùng với đó là rất nhiều tính năng thú vị sẽ giúp người chơi dễ dàng đạt được giải thưởng.</p>

<p>Từ câu 1 đến câu 7, người chơi có thể sử dụng trái tim thêm lượt để tăng cơ hội chiến thắng. Trái tim thêm lượt sẽ xuất hiện khi người chơi giới thiệu bạn bè mới tham gia trò chơi bằng cách nhập mã giới thiệu hoặc tham gia trò chơi 4 lần liên tiếp. Bên cạnh đó, người chơi còn có thể "đầu tư" để mua trái tim thêm lượt trong các phiên bản tiếp theo.
Giải thưởng hiện tại của chương trình đã lên tới 20 triệu đồng chia đều cho các khán giả trả lời đúng 10 câu hỏi của chương trình. Tiền thưởng sẽ được thanh toán theo thông tin cung cấp của người thắng cuộc sau khi chương trình kết thúc.</p>

<p>Với mục tiêu tạo sân chơi “dễ chơi - dễ trúng”, chỉ cần điện thoại thông minh kết nối mạng Internet thì bất cứ ai cũng có thể sẵn sàng tham gia tranh tài ngay lập tức với người chơi khác để giành chiến thắng mà không phải chi trả bất kỳ một khoản chi phí nào trong suốt quá trình diễn ra trò chơi.</p>